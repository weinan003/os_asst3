/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <proc.h>

/*
 * Note! If OPT_DUMBVM is set, as is the case until you start the VM
 * assignment, this file is not compiled or linked or in any way
 * used. The cheesy hack versions in dumbvm.c are used instead.
 *
 * UNSW: If you use ASST3 config as required, then this file forms
 * part of the VM subsystem.
 *
 */


struct addrspace *
as_create(void)
{
	struct addrspace *as;

	as = kmalloc(sizeof(struct addrspace));
	if (as == NULL) {
		return NULL;
	}

    //init pagetable level one
    as->pagetable__ = kmalloc(PAGE_SIZE);
    if(as->pagetable__ == NULL)
    {
        kfree(as);
        return NULL;
    }
    for(uint32_t id = 0;id < 1024;id++)
        as->pagetable__[id] = NULL;

    //init regions dummy head
    as->as_regions = kmalloc(sizeof(struct regions));
    if(as->as_regions == NULL)
    {
        kfree(as->pagetable__);
        kfree(as);
        return NULL;
    }

    as->as_regions->vaddr = 0;
    as->as_regions->npages = 0;
    as->as_regions->permission = 0;
    as->as_regions->next_ = NULL;

	return as;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *newas;

	newas = as_create();
	if (newas==NULL) {
		return ENOMEM;
	}

	/*
	 * Write this.
	 */
    //copy regions
    struct regions* o_region = old->as_regions;
    struct regions* n_region = newas->as_regions;
    while(o_region->next_)
    {
        o_region = o_region->next_;

        //allocate new region and sanity check
        n_region->next_ = kmalloc(sizeof(struct regions));
        n_region = n_region->next_;
        if(n_region == NULL)
        {
            as_destroy(newas);
            return ENOMEM;
        }

        n_region->next_ = NULL;
        n_region->vaddr = o_region->vaddr;
        n_region->npages = o_region->npages;
        n_region->permission = o_region->permission;
    }

    //copy pagetable
    as_prepare_load(newas);
    paddr_t** o_pagetable = old->pagetable__;
    paddr_t** n_pagetable = newas->pagetable__;
    uint32_t lvl1_id,lvl2_id;
    for(lvl1_id = 0;lvl1_id < 1024;lvl1_id ++)
    {
        if(o_pagetable[lvl1_id])
        {
            n_pagetable[lvl1_id] = kmalloc(PAGE_SIZE);
            if(n_pagetable[lvl1_id] == NULL)
            {
                as_destroy(newas);
                return ENOMEM;
            }

            for(lvl2_id = 0;lvl2_id < 1024;lvl2_id ++)
            {
                if(o_pagetable[lvl1_id][lvl2_id])
                {
                    n_pagetable[lvl1_id][lvl2_id] = alloc_ppages(1);
                    if(n_pagetable[lvl1_id][lvl2_id] == 0)
                    {
                        as_destroy(newas);
                        return ENOMEM;
                    }

                    //move infor from old physical memmory to new
                    memmove((void*)PADDR_TO_KVADDR(n_pagetable[lvl1_id][lvl2_id]),(const void *)PADDR_TO_KVADDR(o_pagetable[lvl1_id][lvl2_id] & PAGE_FRAME),PAGE_SIZE);
                    //copy readable,writeable,excutable
                    n_pagetable[lvl1_id][lvl2_id] = n_pagetable[lvl1_id][lvl2_id]|(o_pagetable[lvl1_id][lvl2_id]&0x00000FFF);
                }
                else
                    n_pagetable[lvl1_id][lvl2_id] = 0;
            }
        }
    }

    as_complete_load(newas);
	*ret = newas;
	return 0;
    
}

void
as_destroy(struct addrspace *as)
{
    //free pagetable
    if(as->pagetable__)
    {
        for(uint32_t id;id < 1024;id ++)
        {
            if(as->pagetable__[id])
            {
               for(uint32_t l2_id=0;l2_id < 1024;l2_id ++)
               {
                   if(as->pagetable__[id][l2_id])
                       free_ppages(as->pagetable__[id][l2_id]);
               }
                kfree(as->pagetable__[id]);
            }
        }
        kfree(as->pagetable__);
    }

    //free regions
    if(as->as_regions)
    {
        while(as->as_regions->next_)
        {
            struct regions* p = as->as_regions->next_;
            as->as_regions->next_ =as->as_regions->next_->next_;
            kfree(p);
        }
        kfree(as->as_regions);

    }

	kfree(as);
}

void
as_activate(void)
{
    int i,spl;
	struct addrspace *as;

	as = proc_getas();
	if (as == NULL) {
		/*
		 * Kernel thread without an address space; leave the
		 * prior address space in place.
		 */
		return;
	}

    spl = splhigh();

    for(i = 0;i < NUM_TLB;i ++)
    {
        tlb_write(TLBHI_INVALID(i),TLBLO_INVALID(),i);
    }
    splx(spl);
}

void
as_deactivate(void)
{
    int i,spl;
	struct addrspace *as;

	as = proc_getas();
	if (as == NULL) {
		/*
		 * Kernel thread without an address space; leave the
		 * prior address space in place.
		 */
		return;
	}

    spl = splhigh();

    for(i = 0;i < NUM_TLB;i ++)
    {
        tlb_write(TLBHI_INVALID(i),TLBLO_INVALID(),i);
    }
    splx(spl);
}

/*
 * Set up a segment at virtual address VADDR of size MEMSIZE. The
 * segment in memory extends from VADDR up to (but not including)
 * VADDR+MEMSIZE.
 *
 * The READABLE, WRITEABLE, and EXECUTABLE flags are set if read,
 * write, or execute permission should be set on the segment. At the
 * moment, these are ignored. When you write the VM system, you may
 * want to implement them.
 */
int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t memsize,
		 int readable, int writeable, int executable)
{
    //find last node in regions list
    struct regions* pRg = as->as_regions;
    while(pRg->next_)
        pRg = pRg->next_;

    pRg->next_ = kmalloc(sizeof(struct regions));
    if(pRg->next_ == NULL)
        return ENOMEM;

    pRg = pRg->next_;

    pRg->vaddr = 0;
    pRg->npages = 0;
    pRg->permission = 0;
    pRg->next_ = NULL;
    //prevent vaddr not start in the begin of page
    memsize += vaddr & 0x00000FFF;
    vaddr = vaddr & 0xFFFFF000;

    pRg->vaddr = vaddr;
    pRg->npages = (memsize + PAGE_SIZE - 1)/PAGE_SIZE;
    
    pRg->permission = (uint32_t)(readable|writeable|executable);

	return 0;
}

int
as_prepare_load(struct addrspace *as)
{
    as->as_regions->permission = 0x7;
    as_activate();
	return 0;
}

int
as_complete_load(struct addrspace *as)
{
    as->as_regions->permission = 0x0;
    as_activate();
	return 0;
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
    (void)as;
	*stackptr = USERSTACK;
	return 0;
}
