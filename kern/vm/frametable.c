#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>

/* Place your frametable data-structures here 
 * You probably also want to write a frametable initialisation
 * function and call it from vm_bootstrap
 */

static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;
//frametable
struct frame_table_entry* frametable = 0;
//frametable size
uint32_t ft_size_ = 0;

/* Note that this function returns a VIRTUAL address, not a physical 
 * address
 * WARNING: this function gets called very early, before
 * vm_bootstrap().  You may wish to modify main.c to call your
 * frame table initialisation function, or check to see if the
 * frame table has been initialised and call ram_stealmem() otherwise.
 */

vaddr_t alloc_kpages(unsigned int npages)
{
	paddr_t addr;

    if(frametable == 0)
    {
        //before frametable initialize
        spinlock_acquire(&stealmem_lock);
        addr = ram_stealmem(npages);
        spinlock_release(&stealmem_lock);

        if(addr == 0)
            return 0;
    }
    else
    {
        addr = alloc_ppages(npages);
    }

	return addr? PADDR_TO_KVADDR(addr) : addr;
}

void free_kpages(vaddr_t addr)
{
    //translate virtual memory to physical memory
    paddr_t paddr = KVADDR_TO_PADDR(addr);
    //translate to frametable id
    free_ppages(paddr);
}

void free_ppages(paddr_t paddr)
{
    uint32_t id;
    id = paddr>>12;
    //out of memory
    if(id >= ft_size_)
        return;
    
    //check if used
    if(frametable[id].isused == 0)
        return;

    spinlock_acquire(&stealmem_lock);
    frametable[id].isused = 0;
    spinlock_release(&stealmem_lock);
}

paddr_t alloc_ppages(unsigned int npages)
{
    paddr_t addr = 0;
    spinlock_acquire(&stealmem_lock);
    if(npages == 1)
    {
        //singal page allocation
        for(uint32_t id = 0;id < ft_size_;id ++)
        {
            if(frametable[id].isused == 0)
            {
                addr = id << 12;
                frametable[id].isused = 1;
                break;
            }
        }
    }
    spinlock_release(&stealmem_lock);
    return addr;
}

