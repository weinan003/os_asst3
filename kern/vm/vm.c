#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <cpu.h>
#include <spinlock.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <machine/tlb.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <elf.h>

/* Place your page table functions here */

extern struct frame_table_entry* frametable;
extern uint32_t ft_size_;


void vm_bootstrap(void)
{
    //get physical memory boundary
    paddr_t ram_top = ram_getsize();
    //get kernal code boundary
    paddr_t ram_bottom = ram_getfirstfree();

    //calculate frametable size
    paddr_t location = ram_top - (ram_top/PAGE_SIZE) * sizeof(struct frame_table_entry);
    //transfer physical memory address to virtual address
    frametable = (struct frame_table_entry*) PADDR_TO_KVADDR(location);

    //perpare initial frametable
    ft_size_ = ram_top >> 12;
    uint32_t kernel_end_id = ram_bottom >> 12;
    uint32_t ft_start_id = location >> 12;
    for(uint32_t id = 0;id < ft_size_;id++)
    {
        if(id < kernel_end_id || id >= ft_start_id)
        {
            frametable[id].isused = 1;
        }
        else
        {
            frametable[id].isused = 0;
        }
    }
}

int
vm_fault(int faulttype, vaddr_t faultaddress)
{
    paddr_t paddr;
	uint32_t ehi, elo;
    struct addrspace *as;
    int spl;
    struct regions* pRg;
    uint32_t vaddr_id; 
    uint32_t pt_lvl2_id;
    uint32_t pt_lvl1_id;
    int i;

	faultaddress &= PAGE_FRAME;

	switch (faulttype) {
	    case VM_FAULT_READONLY:
		/* We always create pages read-write, so we can't get this */
        return EFAULT;
	    case VM_FAULT_READ:
	    case VM_FAULT_WRITE:
		break;
	    default:
		return EINVAL;
	}

	if (curproc == NULL) {
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = proc_getas();
	if (as == NULL) {
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* Assert that the address space has been set up properly. */
    KASSERT(as->as_regions !=0);
    pRg = as->as_regions->next_;
    while(pRg)
    {
        KASSERT(pRg->vaddr != 0);
        KASSERT((pRg->vaddr & PAGE_FRAME) == pRg->vaddr);
        pRg = pRg->next_;
    }

    //Look up pagetable
    paddr = 0;
    vaddr_id = faultaddress >> 12;
    pt_lvl1_id = vaddr_id >> 10;
    pt_lvl2_id = vaddr_id & 0x3FF;
    if(as->pagetable__[pt_lvl1_id] != NULL )
    {
        if(as->pagetable__[pt_lvl1_id][pt_lvl2_id] != 0)
        {
            spl = splhigh();
            paddr = as->pagetable__[pt_lvl1_id][pt_lvl2_id];


            for (i=0; i<NUM_TLB; i++) {
                tlb_read(&ehi, &elo, i);
                if (elo & TLBLO_VALID) {
                    continue;
                }
                ehi = faultaddress;

                if(as->as_regions->permission)
                    elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
                else
                    elo = paddr ;

                tlb_write(ehi, elo, i);
                splx(spl);
                return 0;
            }

            //if TLB full
            ehi = faultaddress;
            if(as->as_regions->permission)
                elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
            else
                elo = paddr ;

            tlb_random(ehi,elo);
            splx(spl);
            return 0;
        }
    }
    //if can not find in pagetable find stack and regions  
    if(paddr == 0)
    {
        vaddr_t stacktop = USERSTACK;
        vaddr_t stackbottom = USERSTACK - VM_STACKPAGES * PAGE_SIZE;
        if(stacktop > faultaddress && faultaddress >= stackbottom)
        {
            if(as->pagetable__[pt_lvl1_id] == NULL)
            {
                as->pagetable__[pt_lvl1_id] = (void*)kmalloc(PAGE_SIZE);
                if(as->pagetable__[pt_lvl1_id] == NULL)
                    return EFAULT;
                for(uint32_t i = 0;i < 1024;i++)
                    as->pagetable__[pt_lvl1_id][i] = 0;
            }
            paddr = alloc_ppages(1);
            if(paddr == 0)
                return EFAULT;

            //data on the stack which protection bit always readable and writeable
            paddr |= TLBLO_DIRTY | TLBLO_VALID;

            as->pagetable__[pt_lvl1_id][pt_lvl2_id] = paddr;
        }
        else
        {
            pRg = as->as_regions;
            while(pRg->next_)
            {
                pRg = pRg->next_;
                if(pRg->vaddr <= faultaddress && faultaddress < pRg->vaddr + PAGE_SIZE *pRg->npages)
                {
                    if(as->pagetable__[pt_lvl1_id] == NULL)
                    {
                        as->pagetable__[pt_lvl1_id] = (void*)kmalloc(PAGE_SIZE);
                        if(as->pagetable__[pt_lvl1_id] == NULL)
                            return EFAULT;
                        for(uint32_t i = 0;i < 1024;i++)
                            as->pagetable__[pt_lvl1_id][i] = 0;
                    }
                    paddr = alloc_ppages(1);
                    if(paddr == 0)
                        return EFAULT;


                    if(pRg->permission & PF_W)
                        paddr |= TLBLO_DIRTY | TLBLO_VALID;
                    else
                        paddr |= TLBLO_VALID;

                    as->pagetable__[pt_lvl1_id][pt_lvl2_id] = paddr;
                    break;
                }
            }
        }
    }

	/* Disable interrupts on this CPU while frobbing the TLB. */
    if(paddr)
    {
        spl = splhigh();

        for (i=0; i<NUM_TLB; i++) {
            tlb_read(&ehi, &elo, i);
            if (elo & TLBLO_VALID) {
                continue;
            }
            ehi = faultaddress;
            //if as_prepare_load is set, all frame page can write
            if(as->as_regions->permission)
                elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
            else
                elo = paddr ;
            tlb_write(ehi, elo, i);
            splx(spl);
            return 0;
        }

        ehi = faultaddress;
        if(as->as_regions->permission)
            elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
        else
            elo = paddr ;

        tlb_random(ehi,elo);
        splx(spl);

        return 0;
    }
    return EFAULT;
}

/*
 *
 * SMP-specific functions.  Unused in our configuration.
 */

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("vm tried to do tlb shootdown?!\n");
}
